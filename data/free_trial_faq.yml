groups:
  - group: "What’s included in a free trial"
    questions:
      - question: What is included in my free trial? What’s excluded?
        answer: >-
          Your free trial includes all features of our [Ultimate tier](https://about.gitlab.com/pricing/); however, free trials exclude support at any level. If you wish to evaluate GitLab for support expertise or SLA performance specifically, please [contact sales](https://about.gitlab.com/sales/) to discuss options.
      - question: What happens after my free trial ends?
        answer: >-
          Your GitLab Ultimate trial lasts 30 days. After this period, you can maintain a GitLab Free account forever or upgrade to a [paid plan](https://about.gitlab.com/pricing/).
  - group: "SaaS vs. Self-Managed"
    questions:
      - question: What is the difference between SaaS and Self-Managed setups?
        answer: >-
          SaaS: We host. No technical setup required, so you don’t have to worry about downloading and installing GitLab yourself.
          Self-Managed: You host. Download and install GitLab on your own infrastructure or in our public cloud environment. Requires Linux experience.
      - question: Are certain features included only in SaaS or Self-Managed?
        answer: >-
          Certain features are only available on Self-Managed. Compare the [full list of features here](https://about.gitlab.com/pricing/self-managed/feature-comparison/).
  - group: Pricing and discounts
    questions:
      - question: Is a credit/debit card required for a free trial?
        answer: >-
          A credit/debit card is not required for customers who do not use GitLab.com CI/CD, bring their own runners, or disable shared runners. However, credit/debit card details are required if you choose to use GitLab.com shared runners. This change was made to discourage abuse of the free pipeline minutes provided on GitLab.com to mine cryptocurrencies - which created performance issues for GitLab.com users. When you provide the card, it will be verified with a one-dollar authorization transaction. No charge will be made and no money will transfer. Learn more [here](https://about.gitlab.com/blog/2021/05/17/prevent-crypto-mining-abuse/).
      - question: How much does a GitLab license cost?
        answer: >-
          Subscription information is listed on our [pricing page](https://about.gitlab.com/pricing/).
      - question: Do you have special pricing for open source projects, startups, or educational institutions?
        answer: >-
          Yes! We provide free Ultimate licenses to qualifying open source projects, startups, and educational institutions. Find out more by visiting our [GitLab for Open Source](https://about.gitlab.com/solutions/open-source/), [GitLab for Startups](https://about.gitlab.com/solutions/startups/), and [GitLab for Education](https://about.gitlab.com/solutions/education/) program pages.
      - question: How do I upgrade from GitLab Free to one of the paid subscriptions?
        answer: >-
          If you want to upgrade from GitLab Free to one of the paid tiers, follow the [guides in our documentation](https://docs.gitlab.com/ee/update/README.html#community-to-enterprise-edition).
  - group: "Installation and migration"
    questions:
      - question: How do I migrate to GitLab from another Git tool?
        answer: >-
          See all the project migration instructions for popular version control systems in [our documentation](https://docs.gitlab.com/ee/user/project/import/index.html).
      - question: How do I install GitLab using a container?
        answer: >-
          Find out about installing GitLab using Docker in [our documentation](https://docs.gitlab.com/omnibus/docker/README.html).
  - group: GitLab integrations
    questions:
      - question: Is GitHost still available?
        answer: >-
          No, we are no longer accepting new customers for GitHost. More information is available in the [GitHost FAQ](https://about.gitlab.com/githost/).
      - question: What tools does GitLab integrate with?
        answer: >-
          GitLab offers a number of third-party integrations. Learn more about available services and how to integrate them in [our documentation](https://docs.gitlab.com/ee/integration/README.html).
