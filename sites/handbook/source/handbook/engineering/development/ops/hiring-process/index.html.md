---
layout: handbook-page-toc
title: Ops Hiring Process
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

We are running with some limited capacity in the recruiting team and we are asking hiring managers to cover

## Hiring Process

The process for Ops is a mix of the current [hiring manager (HM) tasks](/handbook/hiring/talent-acquisition-framework/hiring-manager/) and the [talent acquisition (TA) tasks](/handbook/hiring/talent-acquisition-framework/req-overview/). 

1. [Identify hiring need](/handbook/hiring/talent-acquisition-framework/hiring-manager/#step-1hm-identifying-hiring-need)
    1. Work with Product Manager as they are [DRI of headcount planning](/handbook/engineering/#headcount-planning), Finance, and Talent Acquisition to [include vacancy in the hiring plan](/handbook/hiring/talent-acquisition-framework/req-creation/#adding-vacancies-to-the-hiring-plan-dri-hiring-manager). You should obtain a GitLab Hiring Plan ID (GHP ID)
    1. [Create or Review the Job Family](/handbook/hiring/talent-acquisition-framework/req-creation/#create-or-review-the-job-family)
    1. Work with TA to [open the vacancy in Greenhouse](/handbook/hiring/talent-acquisition-framework/req-creation/#opening-vacancies-in-greenhouse-dri-recruiter)
1. Kickoff
    1. [Create Kickoff](/handbook/hiring/talent-acquisition-framework/req-overview/#step-3-complete-kick-off-session-agree-on-priority-level--complete-a-sourcing-session)
    1. Include label ~section::ops to the kickoff issue so we can track in [this board](https://gitlab.com/gl-talent-acquisition/req-intake/-/boards/3516477?label_name[]=section%3A%3Aops) (confidential) and use it as the single source of thruth (SSOT)
    1. [Complete Kickoff](/handbook/hiring/talent-acquisition-framework/hiring-manager/#step-2hm-complete-kick-off)
1. Setup job in Greenhouse
    1. [Setup scoreboard](/handbook/hiring/talent-acquisition-framework/hiring-manager/#step-3hm-setup-scorecard-and-prepare-interview-team)
    1. Publish job ad
1. Sourcing 
    1. Engage top talent
    1. [Source candidates and review inbound applications](/handbook/hiring/talent-acquisition-framework/hiring-manager/#step-4hm-optional-source-candidates-andor-review-inbound-applications)
    1. [Contact candidates](/handbook/hiring/talent-acquisition-framework/req-overview/#step-4-indentify--engage-top-talent-prospects) 
1. Screening
    1. Schedule screening calls (3-6 per week) 
    1. Tag TA on the candidates Greenhouse profile so they can support with screening calls. and work with TA in case they have capacity to do the screening
    1. [Screening](/handbook/hiring/talent-acquisition-framework/req-overview/#screening). Don't forget to introduce the TA to the prospect at the start of the team interview stage so they can build a relationship with the candidate and ensure a consistent experience.
1. [Continuous check-in](/handbook/hiring/talent-acquisition-framework/req-overview/#step-5-weekly-check-in-issue)
1. [Assessment](/handbook/hiring/talent-acquisition-framework/req-overview/#assessment)
1. Ping TA to [Schedule interviews](/handbook/hiring/talent-acquisition-framework/req-overview/#team-interview) so they can manage the candidate throughout the interview stages
1. Perform interviews
1. [Complete feedback](/handbook/hiring/talent-acquisition-framework/hiring-manager/#step-5hm-hiring-team-to-complete-feedback-in-greenhouse)
1. References
    1. [Request references](/handbook/hiring/talent-acquisition-framework/req-overview/#step-7-references-and-background-check)
    1. [Complete references](/handbook/hiring/talent-acquisition-framework/hiring-manager/#step-6hm-complete-references)
1. [Justification](/handbook/hiring/talent-acquisition-framework/req-overview/#step-8-justification-engineering-only)
